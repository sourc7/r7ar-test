off_t get_file_size(const char *file_name)
{
        struct stat st;
        stat(file_name, &st);
        return(st.st_size);
}
