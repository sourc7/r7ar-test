################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../src/buffer.o \
../src/checkpoint.o \
../src/compare.o \
../src/create.o \
../src/delete.o \
../src/exclist.o \
../src/exit.o \
../src/extract.o \
../src/incremen.o \
../src/list.o \
../src/misc.o \
../src/names.o \
../src/sparse.o \
../src/suffix.o \
../src/system.o \
../src/tar.o \
../src/transform.o \
../src/unlink.o \
../src/update.o \
../src/utf8.o \
../src/warning.o \
../src/xattrs.o \
../src/xheader.o 

C_SRCS += \
../src/buffer.c \
../src/checkpoint.c \
../src/compare.c \
../src/create.c \
../src/delete.c \
../src/exclist.c \
../src/exit.c \
../src/extract.c \
../src/incremen.c \
../src/list.c \
../src/misc.c \
../src/names.c \
../src/sparse.c \
../src/suffix.c \
../src/system.c \
../src/tar.c \
../src/transform.c \
../src/unlink.c \
../src/update.c \
../src/utf8.c \
../src/warning.c \
../src/xattrs.c \
../src/xheader.c 

OBJS += \
./src/buffer.o \
./src/checkpoint.o \
./src/compare.o \
./src/create.o \
./src/delete.o \
./src/exclist.o \
./src/exit.o \
./src/extract.o \
./src/incremen.o \
./src/list.o \
./src/misc.o \
./src/names.o \
./src/sparse.o \
./src/suffix.o \
./src/system.o \
./src/tar.o \
./src/transform.o \
./src/unlink.o \
./src/update.o \
./src/utf8.o \
./src/warning.o \
./src/xattrs.o \
./src/xheader.o 

C_DEPS += \
./src/buffer.d \
./src/checkpoint.d \
./src/compare.d \
./src/create.d \
./src/delete.d \
./src/exclist.d \
./src/exit.d \
./src/extract.d \
./src/incremen.d \
./src/list.d \
./src/misc.d \
./src/names.d \
./src/sparse.d \
./src/suffix.d \
./src/system.d \
./src/tar.d \
./src/transform.d \
./src/unlink.d \
./src/update.d \
./src/utf8.d \
./src/warning.d \
./src/xattrs.d \
./src/xheader.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


