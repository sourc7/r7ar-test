################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../lib/paxerror.o \
../lib/paxexit-status.o \
../lib/paxnames.o \
../lib/prepargs.o \
../lib/rtapelib.o \
../lib/stdopen.o \
../lib/wordsplit.o \
../lib/xattr-at.o 

C_SRCS += \
../lib/paxerror.c \
../lib/paxexit-status.c \
../lib/paxnames.c \
../lib/prepargs.c \
../lib/rtapelib.c \
../lib/stdopen.c \
../lib/wordsplit.c \
../lib/xattr-at.c 

OBJS += \
./lib/paxerror.o \
./lib/paxexit-status.o \
./lib/paxnames.o \
./lib/prepargs.o \
./lib/rtapelib.o \
./lib/stdopen.o \
./lib/wordsplit.o \
./lib/xattr-at.o 

C_DEPS += \
./lib/paxerror.d \
./lib/paxexit-status.d \
./lib/paxnames.d \
./lib/prepargs.d \
./lib/rtapelib.d \
./lib/stdopen.d \
./lib/wordsplit.d \
./lib/xattr-at.d 


# Each subdirectory must supply rules for building sources it contributes
lib/%.o: ../lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


